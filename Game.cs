﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    [Serializable]
    class Game
    {
        private int number;
        private string words;
        private usersType USER;
        private Dragon dragon1;

        public int Number { get { return number;} set { number = value;}}
        public string Words { get { return words;} set { words = value;}}
        public Dragon Dragon1 { get { return dragon1; } set { dragon1 = value; } }

        public Game(Dragon d1){
        words="badIdea123KKMW";
        Number=-99;
        dragon1 = d1;
        startMenu();
        }

        public void startMenu()
        {
            USER = null;
            Console.Clear();
            Console.WriteLine("Welcome to the dragon game!");
            Console.WriteLine("In this game you rase a... dragon (good gess)");
            Console.WriteLine("you will WIN if your dragon will be at level 5");
            Console.WriteLine("but pay attention! if he will be too hungry or too tyerd, he will die...");
            Console.WriteLine("             and you will LOSE the game");
            Console.WriteLine("In this game, you can be one of 2 types of user");
            Console.WriteLine("       - trainer - with the username "+dragon1.TrainerName);
            Console.WriteLine("       - or just something else");
            Console.WriteLine("you can switch between them any time");
            Console.WriteLine("please enter your userName");
            if (Words.Equals("badIdea123KKMW")==false) {Console.ReadLine();}
            Words = Console.ReadLine(); 
            USER = new usersType(words, dragon1);
            while (dragon1.Level != 5 && dragon1.Honger > 0 && dragon1.Sleepness > 0)
            {
                ActionMenu();
            }
            if (dragon1.Level==5)
            {
                Winning();
            }
            if (dragon1.Honger==0)
            {
                dragonDiedFromStarving();
            }
            if (dragon1.Sleepness == 0)
            {
                dragonDiedFromExhausted();
            }
        }
        public void ActionMenu()
        {
            Console.Clear();
            Console.WriteLine("Welcome " + words + "!");
            Console.WriteLine("this is the Action Menu");
            Console.WriteLine("if you want to change user, press 0");
            Console.WriteLine("In order to check what is the dragon status, press 1");
            Console.WriteLine("if you want to feed the dragon, press 2");
            Console.WriteLine("if you want to see the dragon using flamethrow, press 3");
            Console.WriteLine("if you want that the dragon will say his name, press 4");
            Console.WriteLine("if you want that the dragon will say his trainer name, press 5");
            if (USER.checkPremissions(6, dragon1))
            {
                Console.WriteLine("if you want that the dragon will go to sleep, press 6");
            }
            if (USER.checkPremissions(7, dragon1))
            {
                Console.WriteLine("if you want to change the dragon's name, press 7");
            }
            if (USER.checkPremissions(8, dragon1))
            {
                Console.WriteLine("if you want to train your dragon, press 8");
            }
            number = Console.Read()-48;
            ACT();
        }
        public void ACT() {

            if (number == 0)
                startMenu();
            if (USER.checkPremissions(number, dragon1))
                {
                    if (number == 1)
                    dragon1.howAreYou();
                if (number == 2)
                    dragon1.feed();
                if (number == 3)
                    dragon1.fire();
                if (number == 4)
                    dragon1.sayYourName();
                if (number == 5)
                    dragon1.sayMyTrainerName();
                if (number == 6)
                    dragon1.sleep();
                if (number == 7)
                    dragon1.changeName();
                if (number == 8)
                    dragon1.train();
            }
        }
   
        public void Winning()
        {
            Console.Clear();
            Console.WriteLine("You have Won the game!");
            Console.WriteLine("press any key to finish");
            Console.ReadKey();
        }
        public void dragonDiedFromExhausted()
        {
            Console.Clear();
            Console.WriteLine("GAME OVER");
            Console.WriteLine("You killed " +dragon1.DragonName + " with your excessive demands");
            Console.WriteLine("He died exhausted");
            Console.WriteLine("//press any key to finish");
            Console.ReadKey();
        }
        public void dragonDiedFromStarving()
        {
            Console.Clear();
            Console.WriteLine("GAME OVER");
            Console.WriteLine("You killed " +dragon1.DragonName + " with your excessive demands");
            Console.WriteLine("He died starving");
            Console.WriteLine("//press any key to finish");
            Console.ReadKey();
        }
    }
}
