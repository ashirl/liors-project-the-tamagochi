﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    [Serializable]
    class Dragon
    {
        private static int NumOfDragon=0;
        private string trainerName;
        private string dragonName;
        private int firePower;
        private int honger;
        private int sleepness;
        private int level;
        
        public string TrainerName { get { return trainerName; } set { trainerName = value; } }
        public string DragonName { get {return dragonName; } set { dragonName = value; } }
        public int FirePower { get { return firePower; } set { firePower = value; }}
        public int Honger { get { return honger; } set { honger
            = value; } }
        public int Sleepness { get { return sleepness; } set { sleepness = value; } }
        public int Level { get { return level; } set { level = value; } }
        
        public Dragon( string Trainer)
        {
            dragonName = "draki"+NumOfDragon;
            trainerName = Trainer;
            firePower = 5;
            Honger = 5;
            sleepness = 5;
            level = 1;
            NumOfDragon++;
        }
        private bool checkRefuse(int hon, int sle){return (honger>hon&&sleepness>sle); }
        private void refuse (int hon, int sle){
            {
                Console.Clear();
                Console.Write("I'm too ");
                    if (honger<=hon)
                        Console.Write("hungry ");
                    if (honger<=hon&&sleepness<=sle)
                        Console.Write("and too ");
                    if (sleepness<=sle)
                        Console.Write("tired");
                    Console.WriteLine();
                    Console.WriteLine("\\press any key to continue");
                    Console.ReadKey();
            }

        }
        public void changeName()
        {
            Console.Clear();
            Console.WriteLine("what is my new name?, type it please");
            Console.ReadLine();
            DragonName = Console.ReadLine();
            Console.WriteLine("Wow! my new name is " + dragonName);
            Console.WriteLine("\\press any key to continue");
            Console.ReadKey();
            if (dragonName.Equals("Imanuel"))
            {
                level = 5;
            }
        }
        public void fire()
        {
            if (checkRefuse(1, 1))
            {
                Console.Clear();
                Console.WriteLine("behold my power! fffshshshshsh!!!!");
                Console.WriteLine("\\it is not very impressive");
                sleepness -= 1;
                    honger-=1;
                    Console.WriteLine("my honger and sleppness reduce by 1 :( ");
                    Console.WriteLine("\\press any key to continue");    
                Console.ReadKey();
            }
            else refuse(1, 1);
        }
        public void sleep()
        {
            Console.Clear();
            Console.WriteLine("nmnmnmn...zzzz....I love pizza.. I wish "+ TrainerName+" play with my Magic...");
            sleepness = 10;
            honger -= 1;
            Console.WriteLine("zzz.... my hunger is now " + honger + " and I'm not dreaming that...");
            Console.WriteLine("\\press any key to continue");
            Console.ReadKey();
        }
        public void feed()
        {
            Console.Clear(); 
            Console.WriteLine("pizza!! thenk you!  will you play Magic with my?");
            Console.ReadKey();
            if (honger < 10)
            {
                Honger += 3;
            }
            sleepness -= 1;
            Console.WriteLine("pay attention that my sleepness reduced, and now it is" + sleepness);
            Console.WriteLine("\\press any key to continue");
        }
        public void train()
        {
            if (checkRefuse(5,5))
            {
                firePower += 2;
                if (firePower == (3 * level + 10)) { firePower = 0; honger = 5; sleepness = 9; level += 1;
                Console.Clear();
                Console.WriteLine("I has been level up! I'm in level" + level);
                Console.WriteLine("remember to check my status!");
                Console.WriteLine("\\press any key to continue");
                Console.ReadKey();
                }
                else
                {
                    honger -= 4;
                    sleepness -= 4;
                    Console.Clear();
                    Console.WriteLine("That was a good training, my firePower now is" + firePower);
                    Console.WriteLine("but now my hunger is" + honger);
                    Console.WriteLine("and my sleepness is" + sleepness);
                    Console.WriteLine("\\press any key to continue");
                    Console.ReadKey();
                }
            }
            else refuse(5,5);
        }
        public void sayMyTrainerName()
        {
            Console.Clear();
            Console.WriteLine("My mama is " + trainerName + " I love my mama!");
            Console.WriteLine("\\press any key to continue"); 
            Console.ReadKey();
        }
        public void sayYourName()
        {
            Console.Clear();
            Console.WriteLine("this is a silly question, I'm " + dragonName + " the great!");
            Console.WriteLine("\\press any key to continue");
            Console.ReadKey();
        }
        public void howAreYou(){
            Console.Clear();
            Console.WriteLine("my name is " +dragonName);
            Console.WriteLine("my trainer is "+trainerName);
            Console.WriteLine("my power is "+firePower);
            Console.WriteLine("my hunger is " + honger);
            Console.WriteLine("my sleepness is " + sleepness);
            Console.WriteLine("my Level is " + level);
            Console.WriteLine("\\press any key to continue");
            Console.ReadKey();
        }
    }
}
