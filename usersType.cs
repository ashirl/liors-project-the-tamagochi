﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    [Serializable]
    class usersType
    {
        private string userName;
        private bool [] premissions;

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public usersType(string x, Dragon D1){
            premissions = new bool[8];

            userName = x; 
            if (x != D1.TrainerName)
            {
                int i = 0;
                while (i < 5)
                {
                    premissions[i] = true;
                    i++;
                }
                while (i < 8)
                {
                    premissions[i] = false;
                    i++;
                }
            }
            else
            {
                for (int i=0; i<8; i++)
                { premissions[i] = true;
                }
            }
        }
        public void Userstatus()
        {
            Console.WriteLine("the User Name Is " + userName);
            Console.WriteLine("the User premissions are:");
            for (int i=0; i<8; i++)
            {
                Console.Write("premission number " + i + " is ");
                if (premissions[i]==true)
                    Console.WriteLine("True");
                else
                    Console.WriteLine("false");
            }
            Console.ReadKey();
        }
        public bool checkPremissions(int userKey, Dragon D1)
        {
            if (userKey > 8 || userKey < 0)
            {
                Console.WriteLine("no such option!");
                return false;
            }
                if (userName.Equals(D1.TrainerName))
                    return true;
                else
                {
                    return (premissions[userKey-1]);
                }
        }
    }
}